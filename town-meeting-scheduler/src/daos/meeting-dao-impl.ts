import { Meeting } from "../entities";
import { MeetingDAO } from "./meeting-dao";
import { connect } from "../connection";
import { MissingResourceError } from "../errors";

/**
 * This is the implementation of the MeetingDAO interface.
 * The functions that were declared in the interface are
 * defined here.
 */

// id, m_location, m_time, topics[]

export class MeetingDAOImpl implements MeetingDAO
{
    async createMeeting(meeting: Meeting): Promise<Meeting>
    {
        const sql:string = "insert into meeting (m_location, m_date, m_time, topics) values ($1, $2, $3, $4) returning id";
        const values = [meeting.m_location, meeting.m_date, meeting.m_time, meeting.topics];
        const result = await connect.query(sql, values);
        meeting.id = result.rows[0].id;
        return meeting;
    }

    async getAllMeetings(): Promise<Meeting[]>
    {
        const sql:string = "select * from meeting";
        const result = await connect.query(sql);
        const meetings:Meeting[] = [];

        for (const row of result.rows)
        {
            const meeting:Meeting = new Meeting(row.id, row.m_location, row.m_date, row.m_time, row.topics);
            meetings.push(meeting);
        }

        return meetings;
    }

    async getMeetingByID(id: number): Promise<Meeting>
    {
        const sql:string = "select * from meeting where id = $1";
        const values = [id];
        const result = await connect.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The meeting with id ${id} does not exist.`);
        }

        const row = result.rows[0];
        const meeting:Meeting = new Meeting(row.id, row.m_location, row.m_date, row.m_time, row.topics);
        return meeting;
    }

    async updateMeeting(meeting: Meeting): Promise<Meeting>
    {
        const sql:string = "update meeting set m_location = $1, m_date = $2, m_time = $3, topics = $4 where id = $5";
        const values = [meeting.m_location, meeting.m_date, meeting.m_time, meeting.topics, meeting.id];
        const result = await connect.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The meeting with id ${meeting.id} does not exist.`);
        }

        return meeting;
    }

    async deleteMeetingByID(id: number): Promise<boolean>
    {
        const sql:string = "delete from meeting where id = $1";
        const values = [id];
        const result = await connect.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The meeting with id ${id} does not exist.`);
        }

        return true;
    }
}