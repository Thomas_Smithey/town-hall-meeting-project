import { Meeting } from "../entities";
export interface MeetingDAO
{
    // CREATE
    createMeeting(meeting: Meeting): Promise<Meeting>;

    // READ
    getAllMeetings(): Promise<Meeting[]>;
    getMeetingByID(id: number): Promise<Meeting>;

    // UPDATE
    updateMeeting(meeting: Meeting): Promise<Meeting>;
    
    // DELETE
    deleteMeetingByID(id: number): Promise<boolean>;
}