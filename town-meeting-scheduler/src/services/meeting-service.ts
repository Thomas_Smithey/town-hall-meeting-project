import { Meeting } from "../entities";

export default interface MeetingService {
    registerMeeting(meeting: Meeting): Promise<Meeting>;

    retrieveAllMeetings(): Promise<Meeting[]>;

    retrieveMeetingByID(id: number): Promise<Meeting>;

    modifyMeeting(meeting: Meeting): Promise<Meeting>;

    removeMeetingByID(id: number): Promise<boolean>;
}