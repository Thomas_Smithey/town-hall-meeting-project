import Toolbar from "@mui/material/Toolbar";
import Box from "@mui/system/Box";

export default function DisplayMainBody(){
    return(
    <>
    <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <Toolbar />
    </Box>
    </>
    );
}