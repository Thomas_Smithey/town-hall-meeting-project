import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Toolbar from "@mui/material/Toolbar";
import Box from "@mui/system/Box";
import { Link } from "react-router-dom";
import AddBoxIcon from '@mui/icons-material/AddBox';
import PreviewIcon from '@mui/icons-material/Preview';
import DescriptionIcon from '@mui/icons-material/Description';
import FormControl from "@mui/material/FormControl";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Radio from "@mui/material/Radio";
import Divider from "@mui/material/Divider";
import { Grid, TextField, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { types } from "../types-of-issues";
import { useDispatch, useSelector } from "react-redux";
import { IssueAPI } from "../apis/issues-api";
import { MeetingAPI } from "../apis/meetings-api";
import { DateFormatter } from "../helper-classes/date-formatter";

const drawerWidth = 240;


export default function SideMenu(){
  // Redux store hooks
  const dispatch = useDispatch();
  const currentTownIssueReviewState = useSelector((state:any)=>state); 
  console.log(currentTownIssueReviewState);

  // Filter issues form values
  const dateOfIssueInput = React.useRef<any>(null);
  const dateOfPosted = React.useRef<any>(null);
  const [reviewedRadioValue, setReviewedRadioValue] = React.useState(false);
  const [highlightedRadioValue, setHighlightedRadioValue] = React.useState(false);
  let typeInput = ''; 
  
  // Sets the issue type selected in the form
  const getIssueType = (type:any) => {
    clearRadioButtons();
    console.log(type);
    typeInput = type;
    getFilteredIssues('type', typeInput);
  };

  // Get report
  useEffect(()=>{
    getReport();
  }, []);

  const getIssuesByDateOfIssue = ()=>{
    clearRadioButtons();
    console.log(new Date(dateOfIssueInput.current.value));
    const data = dateOfIssueInput.current.value;
    getFilteredIssues('dateOfIssue', data);
  };

  const getIssuesByDateOfPosted = () => {
    clearRadioButtons();
    console.log(new Date(dateOfPosted.current.value));
    const data = dateOfPosted.current.value;
    getFilteredIssues('dateOfPosted', data);
  };

  const getIssuesByReview = () => {
    clearRadioButtons();
    setReviewedRadioValue(true);

    IssueAPI.getIssuesByReviewed('true')
    .then((response)=>{
      console.log(response);
      const data = response.data;
      dispatch({type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
      dispatch({type: 'updateIssueFilterType', issueFilterType: 'all'});
      dispatch({type: 'updateFilterKind', filterKind: 'isReviewed'});
      dispatch({type: 'updateFilterDate', filterDate: 'none'});
      // dispatch({type: 'updateFilterDate', filterDate: DateFormatter.todayFormattedDateString()});
    })
    .catch((error)=>{console.log(error);
    });
    
  };

  const getIssuesByHighlighted = () => {
    clearRadioButtons();
    setHighlightedRadioValue(true);

    IssueAPI.getIssuesByHighlighted('true')
    .then((response)=>{
      console.log(response);
      const data = response.data;
      dispatch({type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
      dispatch({type: 'updateIssueFilterType', issueFilterType: 'all'});
      dispatch({type: 'updateFilterKind', filterKind: 'isHighlighted'});
      dispatch({type: 'updateFilterDate', filterDate: 'none'});
      // dispatch({type: 'updateFilterDate', filterDate: DateFormatter.todayFormattedDateString()});
    })
    .catch((error)=>{console.log(error);
    });
    
  };

  const clearRadioButtons = () => {
    setReviewedRadioValue(false);
    setHighlightedRadioValue(false);
  };

  const getFilteredIssues = (filter:string, value:any) => {
    switch (filter) {
      case 'type':
          console.log(filter);
          console.log(value);
          IssueAPI.getIssuesByType(value)
          .then((response)=>{
            console.log(response.data);
            const data = response.data;
            dispatch({type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
            dispatch({type: 'updateIssueFilterType', issueFilterType: value});
            dispatch({type: 'updateFilterKind', filterKind: filter});
          })
          .catch((error)=>{console.log(error);
          });
        break;
      case 'dateOfIssue':
        IssueAPI.getIssuesByDateOfIssue(DateFormatter.formatDateString(value))
        .then((response)=>{
          console.log(response.data);
          const data = response.data;
          dispatch({type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
          dispatch({type: 'updateIssueFilterType', issueFilterType: 'all'});
          dispatch({type: 'updateFilterKind', filterKind: filter});
          dispatch({type: 'updateFilterDate', filterDate: DateFormatter.formatDateString(value)});
        })
        .catch((error)=>{console.log(error);
        });
        break;
      case 'dateOfPosted':
        IssueAPI.getIssuesByDateOfPosted(DateFormatter.formatDateString(value))
        .then((response)=>{
          console.log(response.data);
          const data = response.data;
          dispatch({type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
          dispatch({type: 'updateIssueFilterType', issueFilterType: 'all'});
          dispatch({type: 'updateFilterKind', filterKind: filter});
          dispatch({type: 'updateFilterDate', filterDate: DateFormatter.formatDateString(value)});
        })
        .catch((error)=>{console.log(error);
        });
        break;
      default:
        break;
    }
  };

  const getAllIssues = ()=>{
    clearRadioButtons();
    IssueAPI.getIssues()
    .then((response)=>{
      console.log(response.data);
      const data = response.data;
      dispatch({type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
      dispatch({type: 'updateIsReviewIssueView', isReviewIssueView: true });
      dispatch({type: 'updateFilterKind', filterKind: 'all'});
    })
    .catch((error)=>{console.log(error);
    })

  };

  const getAllMeetings = ()=>{
    MeetingAPI.getMeetings()
    .then((response)=>{
      const data = response.data;
      dispatch({type: 'updateMeetings', meetings: data });
    })
    .catch((error)=>{console.log(error);
    })

  };

  const getReport = ()=>{
    IssueAPI.getIssuesReport()
    .then((response)=>{
      console.log(response.data);
      const data = response.data;
      dispatch({type: 'updateIssuesReport', issuesReport: data });
    })
    .catch((error)=>{
      console.log(error); 
    });
    
  };

  function setItem(i: any, text: any) {
    switch (i) {
      case 0:
        return <Link key={i} className="menuIcon" to="/create-issue"><ListItem button key={text} onClick={() => { dispatch({ type: 'updateIsReviewIssueView', isReviewIssueView: false }) }}>
          <ListItemIcon ><AddBoxIcon /></ListItemIcon>
          <ListItemText primary={text} />
        </ListItem>
        </Link>
      case 1:
        return <Link key={i} className="menuIcon" to="/view-meetings"><ListItem button key={text} onClick={getAllMeetings}>
          <ListItemIcon ><PreviewIcon /></ListItemIcon>
          <ListItemText primary={text} />
        </ListItem>
        </Link>
      case 2:
        return <Link key={i} className="menuIcon" to="/create-meeting"><ListItem button key={text} onClick={() => { dispatch({ type: 'updateIsReviewIssueView', isReviewIssueView: false }) }}>
          <ListItemIcon ><AddBoxIcon /></ListItemIcon>
          <ListItemText primary={text} />
        </ListItem>
        </Link>
      case 3:
        return <Link key={i} className="menuIcon" to="/review-issues"><ListItem button key={text} onClick={getAllIssues} >
          <ListItemIcon ><PreviewIcon /></ListItemIcon>
          <ListItemText primary={text} />
        </ListItem>
        </Link>
      case 4:
        return <Link key={i} className="menuIcon" to="/issues-report"><ListItem button key={text} onClick={getReport} >
          <ListItemIcon ><DescriptionIcon /></ListItemIcon>
          <ListItemText primary={text} />
        </ListItem>
        </Link>
      default:
        break;
    }
  }


  // Filter issues form 
  function IssueFilter(){
    if(currentTownIssueReviewState.isReviewIssueView){
      return(
      <>
      <Divider />
      <FormControl component="fieldset">
      <RadioGroup
        aria-label="gender"
        defaultValue="female"
        name="radio-buttons-group"
      >
        <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="flex-start"
        spacing={1}
      >
        <Grid item xs={8}>
        <Typography variant="button" display="block" gutterBottom mt={2}>
          Filter Issues
        </Typography>
        </Grid>
        <Grid item xs={8}>
        <TextField
          variant="standard"
          select
          fullWidth
          onChange={(e) =>{getIssueType(e.target.value)}}
          SelectProps={{
          native: true,
          }}
      >
          {types.map((option) => (
          <option key={option} value={option}>
              {option}
          </option>
          ))}
        </TextField>
        </Grid>
        <Grid item xs={8}>
        <TextField
          required
          margin="dense"
          id="recipient"
          type="date"
          inputRef={dateOfIssueInput}
          variant="standard"
          fullWidth
          onChange={getIssuesByDateOfIssue}
          helperText="Date of issue"
          
        />
        </Grid>
        <Grid item xs={8}>
        <TextField
          required
          margin="dense"
          id="recipient"
          type="date"
          inputRef={dateOfPosted}
          variant="standard"
          fullWidth
          onChange={getIssuesByDateOfPosted}
          helperText="Date the issue was posted"
        />
        </Grid>
        <Grid item xs={8}>
        <FormControlLabel value="Reviewed" control={<Radio checked={reviewedRadioValue} onChange={getIssuesByReview}/>} label="Reviewed" />
        </Grid>
        <Grid item xs={8}>
        <FormControlLabel value="Highlighted" control={<Radio  checked={highlightedRadioValue} onChange={getIssuesByHighlighted}/>} label="Highlighted" />
        </Grid>
        <Grid item xs={8}></Grid>
      </Grid>
      </RadioGroup>
    </FormControl>
    </>);
    }

    return(<></>);
  }

    // Main side menu component
    return(
    <>
    <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: { width: drawerWidth, boxSizing: 'border-box' },
        }}
      >
        <Toolbar />
        <Box sx={{ overflow: 'auto' }}>
          <List>
          {['Create Issue', 'View Meetings', 'Create Meeting', 'Review Issues', 'Issues Report'].map((text, index) => (
              setItem(index, text)
            ))}
          </List>
          <IssueFilter/>
        </Box>
      </Drawer>
    </>
    );
}