import { CardContent, Divider, Typography } from "@mui/material";
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import Toolbar from "@mui/material/Toolbar";
import Box from "@mui/system/Box";
import { useSelector } from "react-redux";

export default function IssuesReport(){
    const currentTownIssueReviewState = useSelector((state:any)=>state); 
    function Report(){
        return(
        <Card>
           <CardContent>
           <Typography gutterBottom variant="h5" component="div">
             Issues Report
           </Typography>
           <Typography gutterBottom variant="subtitle1" component="div">
             Issues by Type
           </Typography>

           <Typography variant="body1" color="text.secondary">
            {`Infrastructure: ${currentTownIssueReviewState.issuesReport.infrastructure}`}
           </Typography>
           <Typography variant="body1" color="text.secondary">
            {`Safety: ${currentTownIssueReviewState.issuesReport.safety}`}
           </Typography>
           <Typography variant="body1" color="text.secondary">
            {`Public Health: ${currentTownIssueReviewState.issuesReport.publicHealth}`}
           </Typography>
           <Typography variant="body1" color="text.secondary">
            {`Pollution: ${currentTownIssueReviewState.issuesReport.pollution}`}
           </Typography>   
           <Typography variant="body1" color="text.secondary">
            {`Noise/Disturbing the peace: ${currentTownIssueReviewState.issuesReport.noiseDisturbing}`}
           </Typography>
           <Typography variant="body1" gutterBottom color="text.secondary">
            {`Other: ${currentTownIssueReviewState.issuesReport.other}`}
           </Typography>
           <Typography gutterBottom variant="subtitle1" component="div">
             Issues in the last 24 Hr and 7 Days
           </Typography>
           <Typography variant="body1" color="text.secondary">
            {`Issues in the last 24 Hours: ${currentTownIssueReviewState.issuesReport.last24Hours.numberOfIssues}`}
           </Typography>
           <Typography gutterBottom variant="body1" color="text.secondary">
           {`Issues in the last 7 Days: ${currentTownIssueReviewState.issuesReport.last7Days.numberOfIssues}`}
           </Typography>
           <Typography gutterBottom variant="subtitle1" component="div">
             Issues Reviewal Status
           </Typography>
           <Typography variant="body1" color="text.secondary">
           {`Issues Reviewed: ${currentTownIssueReviewState.issuesReport.reviewed}`}
           </Typography>
           <Typography variant="body1" color="text.secondary">
           {`Issues Awaiting Reviewal: ${currentTownIssueReviewState.issuesReport.awaitingReviewal}`}
           </Typography>
           </CardContent> 
        </Card>);
    }

    
    return(<>
    <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
    <Toolbar />
    <Grid
         container
         direction="row"
         justifyContent="center"
         alignItems="center"
        >

            <Grid item xs={6} >
                <Report/>
            </Grid>

    </Grid>
    </Box>
    </>);
}