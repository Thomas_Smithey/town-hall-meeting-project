const issues = [
    {description: 'Pothole on main city street',
    location: '123 Main St',
    dateOfIssue: new Date(2021, 20, 9).toDateString(),
    type: 'Infrastructure',
    dateOfPosted: new Date().toDateString(),
    isHighlighted: false,
    isReviewed: false
    },
    {description: 'Loud noise from park',
    location: '456 NE Willsiton Rd',
    dateOfIssue: new Date(2021, 9, 18).toDateString(),
    type: 'Noise',
    dateOfPosted: new Date(2021, 19, 9).toDateString(),
    isHighlighted: false,
    isReviewed: false
    },
    {description: 'Trash on the community park is overflowing',
    location: 'City Town Park',
    dateOfIssue: new Date(2021, 18, 9).toDateString(),
    type: 'Public Health',
    dateOfPosted: new Date ().toDateString(),
    isHighlighted: false,
    isReviewed: false
    },
    {description: 'Factory smoke over city',
    location: '123 Sw Town Rd',
    dateOfIssue: new Date(2021, 14, 9).toDateString(),
    type: 'Pollution',
    dateOfPosted: new Date(2021, 24, 9).toDateString(),
    isHighlighted: true,
    isReviewed: false
    },
    {description: 'Cracks on building',
    location: '9278 new road',
    dateOfIssue: new Date(2021, 6, 8).toDateString(),
    type: 'Infrastructure',
    dateOfPosted: new Date(2021, 14, 9).toDateString(),
    isHighlighted: true,
    isReviewed: false
    }
];

export {issues}