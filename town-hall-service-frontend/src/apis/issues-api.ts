import axios from "axios";

export class IssueAPI {
    static async getIssues(){
        return await axios.get('http://34.70.126.201:80/issues');
       
    }

    static async updateIssue(id:number, issue: any){
        return await axios.put(`http://34.70.126.201:80/issues/${id}`, issue);
    }

    static async getIssuesByType(type:any){
        return await axios.get(`http://34.70.126.201:80/issues?type=${type}`);
    }

    static async getIssuesByDateOfIssue(date:any){
        return await axios.get(`http://34.70.126.201:80/issues?dateofissue=${date}`);
    }

    static async getIssuesByDateOfPosted(date:any){
        return await axios.get(`http://34.70.126.201:80/issues?dateposted=${date}`);
    }

    static async getIssuesByReviewed(value:any){
        return await axios.get(`http://34.70.126.201:80/issues?reviewed=${value}`);
    }

    static async getIssuesByHighlighted(value:any){
        return await axios.get(`http://34.70.126.201:80/issues?highlighted=${value}`);
    }

    static async getIssuesReport(){
        return await axios.get('http://34.70.126.201:80/issues/report');
    };
}