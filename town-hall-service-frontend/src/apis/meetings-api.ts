import axios from "axios";

export class MeetingAPI {
    static async getMeetings(){
        return await axios.get('http://34.136.35.99/meetings');  
    }
    static async getMeetingByID(id:number){
        return await axios.get(`http://34.136.35.99/meetings/${id}`);
    }
    static async createMeeting(meeting:any){
        return await axios.post('http://34.136.35.99/meetings/', meeting)
    }
    static async createMeetingAuth(meeting:any){
        return await axios.post('http://34.136.35.99/meetings/', meeting,
        {headers: {
            'authorization': 'p'
          }})
    }
    static async updateMeeting(id:number, meeting: any){
        return await axios.put(`http://34.136.35.99/meetings/${id}`, meeting)
    }
    static async updateMeetingAuth(id:number, meeting: any){
        return await axios.put(`http://34.136.35.99/meetings/${id}`, meeting,
        {headers: {
            'authorization': 'p'
          }});
    }
    static async deleteMeeting(id:number){
        return await axios.delete(`http://34.136.35.99/meetings/${id}`)
    }
    static async deleteMeetingAuth(id:number){
        return await axios.delete(`http://34.136.35.99/meetings/${id}`,
        {headers: {
            'authorization': 'p'
          }});
    }
}