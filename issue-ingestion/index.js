const {PubSub} = require('@google-cloud/pubsub');
const {Datastore} = require("@google-cloud/datastore");
const express = require('express');
const cors = require('cors');
const bunyan = require('bunyan');
const {LoggingBunyan} = require('@google-cloud/logging-bunyan');

// setup for express app in cloud run
const PORT = process.env.PORT || 3000;
const app = express();
app.use(express.json());
app.use(cors());

// setup for pubsub subscriber
const pubsub = new PubSub({projectId: "town-hall-service"});
const datastore = new Datastore();
// pubsub subscriber function
async function pullIssues()
{
    // sets the up subscription
    const subscription = pubsub.subscription("issue-subscription");
    subscription.on("message", async (message) => 
    {
        // gets the pubsub message and creates an issue out of it
        const newIssue = JSON.parse(message.data);
        const issueKey = datastore.key("Issue");

        const entity =
        {
            key: issueKey,
            data: newIssue
        };
        // sends the newly created issue to datastore and acknowledges the message
        await datastore.insert(entity);
        message.ack();
    });
}
pullIssues();

const cloudLogger = new LoggingBunyan();
const config = {
    name:'issue-submit',
    // streams are where you log the data
    streams:[
        // console.logs the stream info
        {stream:process.stdout, level:'info'},
        // sends logs to GCP
        cloudLogger.stream('info')
    ]
}

const logger = bunyan.createLogger(config);

app.post('/submitissue', async (req, res)=>{
    const issue = req.body;
    // check for correct format
    if(issue.dateOfPosted &&
        issue.dateOfIssue &&
        issue.description &&
        issue.location &&
        issue.type
        ){
            try {
                // add additional properties isHighlighted and isReviewed
                issue.isHighlighted = false;
                issue.isReviewed = false;
                // send the issue to the pubsub topic, log the creation, and send a http response
                const response = await pubsub.topic('issue-topic').publishJSON(issue);
                logger.info(`Issue of type ${issue.type} created at timestamp ${issue.dateOfPosted}`)
                res.status(200).send(response);
            } catch (error) {
                // if an error happen log the error and send an error response
                logger.error(`Error creating issue at ${new Date().toLocaleString()}`)
                res.status(400).send('Error pushing to topic ""issues"');
            }
        }else {
            // if issue doesn't have all required properties, log it and send an error
            logger.warn('A person tried to send an invalid issue.')
            res.status(406).send('The issue does not meet the valid requirements.');
        }
});

app.listen(PORT, ()=>{console.log(`Server started on port ${PORT}`)});



